package print.dao;

import print.common.JobDetails;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

/**
 * Created by Hiral Gosalia on 3/20/16.
 */
public interface StorageDAO {
    public void store(String id, ByteArrayInputStream byteArrayInputStream);
    public InputStream get(String id);
}
