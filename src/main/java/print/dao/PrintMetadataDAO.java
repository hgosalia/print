package print.dao;

import print.common.JobDetails;
import print.common.JobStatus;

import java.util.List;
import java.util.Set;

/**
 * Created by Hiral Gosalia on 3/17/16.
 */
public interface PrintMetadataDAO {

    public JobStatus status(String id);
    public boolean updateStatus(String id, JobStatus status);
    public String addMetadata(JobDetails jobDetails);
    public List<JobDetails> getMetadata(Set<String> ids);
    public JobDetails nextPrintJob();
}
