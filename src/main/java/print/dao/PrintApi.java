package print.dao;

import print.common.JobDetails;
import print.common.JobStatus;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

/**
 * Created by Hiral Gosalia on 3/7/16.
 */
public interface PrintApi {
    public JobStatus status(String id);
    public String request(JobDetails jobDetails);
    public void run();
    public InputStream get(String id);
}
