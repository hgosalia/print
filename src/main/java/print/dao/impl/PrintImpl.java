package print.dao.impl;

import org.apache.commons.collections.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.task.TaskExecutor;
import print.common.JobDetails;
import print.common.JobStatus;
import print.dao.PrintApi;
import print.dao.PrintMetadataDAO;
import print.dao.StorageDAO;
import print.worker.Worker;
import print.worker.WorkerFactory;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Map;

/**
 * Created by Hiral Gosalia on 3/18/16.
 */
public class PrintImpl implements PrintApi {
    private static final Logger log = LoggerFactory.getLogger(PrintImpl.class);

    private PrintMetadataDAO metadataDAO;
    private StorageDAO storageDAO;
    private TaskExecutor taskExecutor;
    private Map<String, Class> workerMap;
    private WorkerFactory workerFactory;

    public void setMetadataDAO(PrintMetadataDAO metadataDAO) {
        this.metadataDAO = metadataDAO;
    }

    public void setStorageDAO(StorageDAO storageDAO) {
        this.storageDAO = storageDAO;
    }

    public void setTaskExecutor(TaskExecutor taskExecutor) {
        this.taskExecutor = taskExecutor;
    }

    public void setWorkerMap(Map<String, Class> workerMap) {
        this.workerMap = workerMap;
    }

    public void setWorkerFactory(WorkerFactory workerFactory) {
        this.workerFactory = workerFactory;
    }

    @Override
    public JobStatus status(String id) {
        return metadataDAO.status(id);
    }

    @Override
    public String request(JobDetails jobDetails) {
        String uniqueId = metadataDAO.addMetadata(jobDetails);
        return uniqueId;
    }

    @Override
    public InputStream get(String id) {
        return storageDAO.get(id);
    }

    @Override
    public void run() {
        log.info("checking for jobs...");

        JobDetails job = metadataDAO.nextPrintJob();
        if (job != null) {
            log.info("executing ['{}']",job.getTitle());

            //spawn another thread
            taskExecutor.execute(new PrintRunner(job));
        } else {
            log.info("No jobs scheduled at this time.");
        }

    }

    private class PrintRunner implements Runnable {
        private JobDetails job;
        PrintRunner(JobDetails job) {
            this.job = job;
        }

        public void run() {
            try {
                //determine what we are printing
                Map<String, String> attrMap = this.job.getAttributes();
                if (MapUtils.isNotEmpty(attrMap)) {
                    Worker worker = getWorker(attrMap.get("subject"));
                    worker.workOnThis(this.job);

                    //should be done at this point
                    metadataDAO.updateStatus(this.job.getId(), JobStatus.COMPLETED);
                }
            } catch (Exception e) {
                //automatically fail this job, log the error
                metadataDAO.updateStatus(this.job.getId(), JobStatus.ERROR);
                log.error("Error with job [{}] : {} ", this.job.getId(), e.getMessage());
            }
        }

        public Worker getWorker(String subject) {
            Class c = workerMap.get(subject);
            Worker worker = workerFactory.createWorker(c);
            return worker;
        }
    }
}
