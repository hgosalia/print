package print.dao.impl;

import io.minio.MinioClient;
import io.minio.errors.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xmlpull.v1.XmlPullParserException;
import print.common.JobDetails;
import print.dao.StorageDAO;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Hiral Gosalia on 3/20/16.
 */
public class MinioStorageDAO implements StorageDAO {

    private static final Logger log = LoggerFactory.getLogger(MinioStorageDAO.class);

    private MinioClient minioClient;
    private String bucket;

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    public void setMinioClient(MinioClient minioClient) {
        this.minioClient = minioClient;
    }

    @Override
    public void store(String id, ByteArrayInputStream byteArrayInputStream) {
        try {
            minioClient.putObject(
                    bucket,
                    id,
                    byteArrayInputStream,
                    byteArrayInputStream.available(),
                    "application/octet-stream");
            byteArrayInputStream.close();
        } catch (InvalidBucketNameException e) {
            log.error("Bucket [{}] does not exist : {}", this.bucket, e.getMessage());
            throw new RuntimeException(e);
        } catch (InvalidKeyException e) {
            log.error("Invalid key : {}",e.getMessage());
            throw new RuntimeException(e);
        } catch (IOException | InsufficientDataException | NoSuchAlgorithmException |
                NoResponseException | XmlPullParserException | ErrorResponseException |
                InvalidArgumentException | InputSizeMismatchException | InternalException e) {
            log.error("Exception storing into bucket : {}", e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public InputStream get(String id) {
        try {
            InputStream is = minioClient.getObject(bucket, id);
            return is;
        } catch (InvalidBucketNameException e) {
            log.error("Bucket [{}] does not exist : {}", this.bucket, e.getMessage());
            throw new RuntimeException(e);
        } catch (InvalidKeyException e) {
            log.error("Invalid key : {}",e.getMessage());
            throw new RuntimeException(e);
        } catch (IOException | InsufficientDataException | NoSuchAlgorithmException |
                NoResponseException | XmlPullParserException | ErrorResponseException |
                InvalidArgumentException | InternalException e) {
            log.error("Exception storing into bucket : {}", e.getMessage());
            throw new RuntimeException(e);
        }
    }
}
