package print.dao.impl;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.quartz.Job;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import print.common.JobDetails;
import print.common.JobStatus;
import print.dao.PrintMetadataDAO;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.*;

/**
 * Created by Hiral Gosalia on 3/17/16.
 */
public class PrintMetadataImpl implements PrintMetadataDAO {

    private static final Logger log = LoggerFactory.getLogger(PrintMetadataImpl.class);

    private static final String SQL_GET_PRINT_JOBS =
            " SELECT a.id, a.user_id, a.title, a.status, a.date \n" +
            " FROM print_job a \n" +
            " WHERE a.id IN (:ids) \n";

    private static final String SQL_GET_STATUS =
            " SELECT a.status \n" +
            " FROM print_job a \n" +
            " WHERE a.id = :id \n";

    private static final String SQL_GET_PRINT_JOB_ATTRS =
            " SELECT a.print_job_id, a.key, a.value \n" +
            " FROM print_job_attr a \n" +
            " WHERE a.print_job_id=:id \n";

    private static final String SQL_UPD_STATUS = "UPDATE print_job SET status = :status WHERE id = :id ";

    private static final String SQL_INS_PRINT_JOB =
            " INSERT INTO print_job (id, user_id, title, status, date) \n" +
            " VALUES (:id, :userId, :title, :status, :date )\n";

    private static final String SQL_INS_PRINT_JOB_ATTRS =
            " INSERT INTO print_job_attrs (print_job_id, key, value) \n" +
                    " VALUES (?, ?, ?) \n";

    private static final String SQL_NEXT_PRINT_JOB =
            " UPDATE print_job\n" +
            " SET status='IN_PROCESS'\n" +
            " WHERE id IN \n" +
            "   (\n" +
            "     SELECT a.id \n" +
            "     FROM print_job a \n" +
            "     WHERE a.status=:status\n" +
            "     ORDER BY a.date ASC\n" +
            "     LIMIT 1\n" +
            "   )\n" +
            " RETURNING id, user_id, title, status, date \n";

    private DataSource dataSource;
    private JdbcTemplate jdbcTemplate;
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplate = new JdbcTemplate(this.dataSource,false);
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(this.jdbcTemplate);
    }

    @Override
    public JobStatus status(String id) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("id", id);
        JobStatus status = this.namedParameterJdbcTemplate.queryForObject(SQL_GET_STATUS, params,
                new RowMapper<JobStatus>() {

                    @Override
                    public JobStatus mapRow(ResultSet resultSet, int i) throws SQLException {
                        return JobStatus.valueOf(resultSet.getString("STATUS"));
                    }
                });
        return status;
    }

    @Override
    public boolean updateStatus(String id, JobStatus status) {
        MapSqlParameterSource params = new MapSqlParameterSource()
                .addValue("id", id)
                .addValue("status", status.name());
        int rowsUpdated = this.namedParameterJdbcTemplate.update(SQL_UPD_STATUS, params);
        return rowsUpdated == 1;
    }

    @Override
    public String addMetadata(JobDetails jobDetails) {
        final UUID id = UUID.randomUUID();

        MapSqlParameterSource params = new MapSqlParameterSource()
                .addValue("id", id.toString())
                .addValue("userId", jobDetails.getUserId())
                .addValue("title",jobDetails.getTitle())
                .addValue("status", jobDetails.getStatus().name())
                .addValue("date", new java.sql.Timestamp(new Date().getTime()), Types.TIMESTAMP);

        int rowsInserted = this.namedParameterJdbcTemplate.update(SQL_INS_PRINT_JOB, params);
        if (rowsInserted == 1) {

            log.info("added job ['{}'] with id [{}]", jobDetails.getTitle(), id);

            Map<String,String> attrMap = jobDetails.getAttributes();
            if (MapUtils.isNotEmpty(attrMap)) {
                Map.Entry<String,String>[] entries = attrMap.entrySet().toArray( new Map.Entry[0] );
                this.jdbcTemplate.batchUpdate(SQL_INS_PRINT_JOB_ATTRS, new BatchPreparedStatementSetter() {

                    @Override
                    public void setValues(PreparedStatement ps, int i) throws SQLException {
                        Map.Entry<String,String> entry = entries[i];
                        ps.setString(1, id.toString());
                        ps.setString(2, entry.getKey());
                        ps.setString(3, entry.getValue());
                    }

                    @Override
                    public int getBatchSize() {
                        return entries.length;
                    }
                });
            }
        }

        return id.toString();
    }

    @Override
    public List<JobDetails> getMetadata(Set<String> ids) {
        MapSqlParameterSource params = new MapSqlParameterSource()
                .addValue("ids", ids);
        List<JobDetails> jobDetailsList
                = this.namedParameterJdbcTemplate.query(SQL_GET_PRINT_JOBS, params, new PrintJobRowMapper());

        return jobDetailsList;
    }

    @Override
    public JobDetails nextPrintJob() {
        MapSqlParameterSource params = new MapSqlParameterSource()
                .addValue("status", JobStatus.SCHEDULED.name());

        List<JobDetails> jobDetailList =
                this.namedParameterJdbcTemplate.query(SQL_NEXT_PRINT_JOB, params, new PrintJobRowMapper());

        if (CollectionUtils.isEmpty(jobDetailList)) {
            log.info("returning empty results");
            return null;
        } else {
            JobDetails job = jobDetailList.get(0);

            //get the attributes associated with this print job
            Map<String,String> attrMap = getPrintJobAttrs(job.getId());
            job.setAttributes(attrMap);

            return job;
        }
    }

    private Map<String,String> getPrintJobAttrs(String id) {
        MapSqlParameterSource params = new MapSqlParameterSource()
                .addValue("id", id);

        Map<String, String> attrMap =
                this.namedParameterJdbcTemplate.query(SQL_GET_PRINT_JOB_ATTRS, params, new ResultSetExtractor<Map>() {

                    @Override
                    public Map extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                        Map<String, String> attrMap = new HashMap<String, String>();
                        while (resultSet.next()) {
                            attrMap.put(resultSet.getString("key"), resultSet.getString("value"));
                        }
                        return attrMap;
                    }
                });
        return attrMap;
    }

    private static class PrintJobRowMapper implements RowMapper<JobDetails> {

        @Override
        public JobDetails mapRow(ResultSet resultSet, int i) throws SQLException {
            String id = resultSet.getString("id");
            Long userId = resultSet.getLong("user_id");
            String title = resultSet.getString("title");
            Date date = new java.util.Date(resultSet.getTimestamp("date").getTime());
            JobStatus jobStatus = JobStatus.valueOf(resultSet.getString("status"));

            JobDetails job = new JobDetails();
            job.setId(id);
            job.setDate(date);
            job.setTitle(title);
            job.setUserId(userId);

            return job ;
        }
    }
}
