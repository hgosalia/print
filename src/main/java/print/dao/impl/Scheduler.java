package print.dao.impl;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;
import print.dao.PrintApi;

/**
 * Created by Hiral Gosalia on 3/20/16.
 */
public class Scheduler extends QuartzJobBean {

    private PrintApi printApi;

    public void setPrintApi(PrintApi printApi) {
        this.printApi = printApi;
    }

    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        printApi.run();
    }
}
