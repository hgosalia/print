package print.worker;

import print.common.JobDetails;

/**
 * Created by Hiral Gosalia on 3/20/16.
 */
public interface Worker {
    public void workOnThis(JobDetails jobDetails);
}
