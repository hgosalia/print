package print.worker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import print.common.JobDetails;
import print.dao.StorageDAO;
import print.worker.impl.CountryWorker;

/**
 * Created by Hiral Gosalia on 3/20/16.
 */
public class WorkerFactory {
    private static final Logger log = LoggerFactory.getLogger(WorkerFactory.class);

    private StorageDAO storageDAO;

    public void setStorageDAO(StorageDAO storageDAO) {
        this.storageDAO = storageDAO;
    }

    public Worker createWorker(Class c) {
        if (c == CountryWorker.class) {
            log.info("instantiating new CountryWorker.");
            CountryWorker w = new CountryWorker();
            w.setStorageDAO(this.storageDAO);
            return w;
        } else {
            throw new IllegalArgumentException("No such worker could be found.");
        }
    }
}
