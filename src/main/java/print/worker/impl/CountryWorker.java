package print.worker.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import print.common.JobDetails;
import print.dao.StorageDAO;
import print.worker.Worker;

import java.io.ByteArrayInputStream;

/**
 * Created by Hiral Gosalia on 3/20/16.
 */
public class CountryWorker implements Worker {

    private static final Logger log = LoggerFactory.getLogger(CountryWorker.class);

    private StorageDAO storageDAO;

    public void setStorageDAO(StorageDAO storageDAO) {
        this.storageDAO = storageDAO;
    }

    @Override
    public void workOnThis(JobDetails jobDetails) {
        log.info("working on [{}] : ['{}']", jobDetails.getId(), jobDetails.getTitle());
        try {
            //do some stuff
            Thread.sleep(25000);

            log.info("storing [{}] ", jobDetails.getId());
            ByteArrayInputStream bis = new ByteArrayInputStream(
                    jobDetails.getTitle().getBytes("UTF-8")
            );

            //persist
            storageDAO.store(jobDetails.getId(), bis);

            log.info("done with [{}]", jobDetails.getId());

        } catch (Exception e ) {
            throw new RuntimeException(e.getMessage());
        }
    }
}
