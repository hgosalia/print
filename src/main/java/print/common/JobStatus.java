package print.common;

import java.io.Serializable;

/**
 * Created by Hiral Gosalia on 3/7/16.
 */
public enum JobStatus implements Serializable {

    COMPLETED,
    ERROR,
    IN_PROCESS,
    SCHEDULED
}
