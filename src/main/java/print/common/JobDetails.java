package print.common;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

/**
 * Created by Hiral Gosalia on 3/7/16.
 */
public class JobDetails implements Serializable{

    @SerializedName("id")
    @Expose
    private String id;          //unique persistence id

    @SerializedName("user_id")
    @Expose
    private Long userId;

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("status")
    @Expose
    private JobStatus status;

    @SerializedName("date")
    @Expose
    private Date date;

    /*
        output =  XLS | DOCX | PDF
        input  =  location to XML doc or SVG
        foreign_id
     */
    @SerializedName("attrs")
    @Expose
    private Map<String,String> attributes;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public JobStatus getStatus() {
        return status;
    }

    public void setStatus(JobStatus status) {
        this.status = status;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Map<String, String> getAttributes() {
        return attributes;
    }

    public void setAttributes(Map<String, String> attributes) {
        this.attributes = attributes;
    }

    @Override
    public String toString() {
        return "JobDetails {" +
                "date=" + date +
                ", id='" + id + '\'' +
                ", userId=" + userId +
                ", title='" + title + '\'' +
                ", status=" + status.name() +
                '}';
    }
}
