package print.common;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.env.Environment;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * Created by Hiral Gosalia on 2/7/16.
 */
public class ProfileYamlFactoryBean extends YamlPropertiesFactoryBean implements ApplicationContextAware  {

    @Autowired
    private ApplicationContext context;
    private List<String> activeProfilesList;

    protected Properties createProperties() {
        final Properties result = new Properties();
        process(new MatchCallback() {
            public void process(Properties properties, Map<String, Object> map) {
                boolean propMatch = false;
                String prop = properties.getProperty("spring.profiles");
                for (String activeProfileProp : activeProfilesList) {
                    if (activeProfileProp.equalsIgnoreCase(prop)) {
                        propMatch = true;
                    }
                }
                if ( properties.getProperty("spring.profiles") == null ||  propMatch ) {
                    result.putAll(properties);
                }
            }
        });
        return result;
    }

    public void setApplicationContext(ApplicationContext context) throws BeansException {
        this.context = context;
        Environment environment = this.context.getEnvironment();
        activeProfilesList = Arrays.asList(environment.getActiveProfiles());
    }
}
