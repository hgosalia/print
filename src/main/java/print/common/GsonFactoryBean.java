package print.common;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by Hiral Gosalia on 2/13/16.
 */
public class GsonFactoryBean {

    public static Gson create() {
        return new GsonBuilder()
                .disableHtmlEscaping()
                .setPrettyPrinting()
                .excludeFieldsWithoutExposeAnnotation()
                .create();
    }

}
