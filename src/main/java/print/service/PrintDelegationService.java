package print.service;

import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import print.common.JobDetails;
import print.common.JobStatus;
import print.dao.PrintApi;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by Hiral Gosalia on 3/7/16.
 */
@Path("/print")
public class PrintDelegationService {

    private static final Logger log = LoggerFactory.getLogger(PrintDelegationService.class);

    private PrintApi printApi;

    private Gson gson;

    public void setPrintApi(PrintApi printApi) {
        this.printApi = printApi;
    }

    public void setGson(Gson gson) {
        this.gson = gson;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/request")
    public Response request(String requestBody) {
        JobDetails jobDetails = gson.fromJson(requestBody, JobDetails.class);
        String id = printApi.request(jobDetails);
        return Response.ok(gson.toJson(id), MediaType.APPLICATION_JSON).build();
    }

    @GET
    @Path("/status/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response status(@PathParam("id") String id) {
        JobStatus jobStatus = printApi.status(id);
        return Response.ok(gson.toJson(jobStatus), MediaType.APPLICATION_JSON).build();
    }

    @GET
    @Path("/get/{id}")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response get(@PathParam("id") String id) {
        final InputStream inputStream = printApi.get(id);
         
        StreamingOutput streamingOutput = new StreamingOutput() {

            @Override
            public void write(OutputStream out) throws IOException, WebApplicationException {
                int length;
                byte[] buffer = new byte[1024];
                while((length = inputStream.read(buffer)) != -1) {
                    out.write(buffer, 0, length);
                }
                out.flush();
                inputStream.close();
            }
        };

        return Response.ok(streamingOutput, MediaType.APPLICATION_OCTET_STREAM)
                .header("Content-Disposition", "attachment; filename=\"" + id + "\"")
                .build();
    }
}
