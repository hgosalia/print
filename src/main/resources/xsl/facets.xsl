<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output indent="yes" />
    <xsl:template match="/">
        <facets>
            <xsl:apply-templates select="//record[starts-with(country,'S')]"/>
        </facets>
    </xsl:template>

    <xsl:template match="record">
        <xsl:copy-of select="."/>
    </xsl:template>

</xsl:stylesheet>